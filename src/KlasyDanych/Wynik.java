package KlasyDanych;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author grobacz
 * klasa danych wyniku
 */
public class Wynik implements Serializable{
    int wynik;
    String gracz;
    LocalDateTime kiedy = LocalDateTime.now();
    public Wynik(String gracz, int wynik){
        this.gracz=gracz;
        this.wynik=wynik;
    }
    public int getWynik(){
        return wynik;
    }
    public String getGracz(){
        return gracz;
    }
    public String getKiedy(){
        return kiedy.format(DateTimeFormatter.ISO_DATE);
    }
    public void setWynik(int wynik){
        this.wynik = wynik;
    }
}
