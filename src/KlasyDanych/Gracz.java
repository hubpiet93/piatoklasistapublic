package KlasyDanych;

import java.io.Serializable;

/**
 *
 * @author grobacz
 * klasa danych gracza
 */
public class Gracz implements Serializable {
    String imie;
    Integer wynik;
    
    public Gracz(){}
    
    public Gracz(String i)
    {
        imie = i;
        wynik = 0;
    }
    
    public String getImie()
    {
        return imie;
    }
    
    public Boolean isEmpty()
    {
        return imie == null;
    }
}
