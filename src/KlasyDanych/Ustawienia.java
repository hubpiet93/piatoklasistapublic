package KlasyDanych;

import java.io.Serializable;

/**
 *
 * @author grobacz
 * klasa danych ustawień
 */
public class Ustawienia implements Serializable{
    int glosnosc;
    Boolean klawisze;
    int poziomTrudnosci;
    int limit;
    public Ustawienia(int g,Boolean k,int p,int l){
        glosnosc=g;
        klawisze=k;
        poziomTrudnosci=p;
        limit=l;
    }
    public int getGlosnosc(){
        return glosnosc;
    }
    public Boolean getKlawisze() {
        return klawisze;
    }

    public int getPoziomTrudnosci() {
        return poziomTrudnosci;
    }

    public int getLimit() {
        return limit;
    }
}
