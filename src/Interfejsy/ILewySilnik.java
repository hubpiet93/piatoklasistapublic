package Interfejsy;

/**
 * Podinterfejs z metodami powiązanymi z operacjami lewego panelu
 * @author grobacz
 */
public interface ILewySilnik {
    /**
     * Sprawdź czy aktualnie wyświetla się pytanie
     * @return true jeśli wyświetla się pytanie
     */
    Boolean isPytanie();
    
    // metody przekazujące wywołąnie do operacji Pytanie
    void sciagaj();
    void patrzNaGotowiec();
    
    // akcesory ratunku, gotowca i ściągi
    void disableRatunek();
    Boolean isSciaga();
    Boolean isGotowiec();
    Boolean isRatunek();
}
