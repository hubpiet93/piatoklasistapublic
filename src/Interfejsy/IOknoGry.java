package Interfejsy;

import javax.swing.JFrame;

/**
 * Podinterfejs z metodami powiązanymi z oknem gry
 * @author grobacz
 */
public interface IOknoGry {
    
    /**
     * Metoda zwraca utworzone przez konstruktor okno gry
     * @return Okno gry
     */
    JFrame getOknoGry();
    
    // metoda wywołana przy uruchomieniu
    void setWholeOkno();
    
    // metoda uniwersalna zmieniająca zawartość okna gry na bieżącą operację
    // składane są panele górny, lewy i panel bieżącej operacji z aktualnymi danymi
    void setOkno(PanelInit operacja);
    
    // metoda upraszczająca zapis
    // aktualizuje komponenty wizualne okna
    void refresh();
    
    
}
