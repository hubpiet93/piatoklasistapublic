/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfejsy;

import BazaDanych.IDB;
import KlasyDanych.Gracz;
import pk.projekt.e2.sound.contract.IAudioManager;
import KlasyDanych.Ustawienia;
import java.util.List;
import java.util.Map;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author grobacz
 */
public interface ISilnikGry extends ILewySilnik, IOknoGry {
    
    
    /**
     * Metoda zwraca uchwyt do komponentu audio
     * @return uchwyt do komponentu audio
     */
    IAudioManager audioManager();
    
    /**
     * Metoda zwraca uchwyt do objektu ustawień
     * @return uchwyt do objektu ustawień
     */
    Ustawienia ustawienia();
    
    /**
     * Metoda zwraca uchwyt do objektu bazy
     * @return uchwyt do objektu bazy
     */
    IDB db();
    
    /**
     * Metoda zwraca uchwyt do objektu gracza
     * @return uchwyt do objektu gracza
     */
    Gracz gracz();
    
    /**
     * Metoda zwraca uchwyt do objektu generatora decyzji
     * @return uchwyt do objektu generatora decyzji
     */
    generator.IDecyzje generatorDecyzji();
    
    /**
     * Metoda zwraca uchwyt do kategorii pytania
     * @return uchwyt do kategorii pytania
     */
    String kategoria();
    
    /**
     * Setter kategorii
     * @param k nazwa kategorii
     */
    void kategoria(String k);
    
    /**
     * Metoda zwraca uchwyt do numeru pomocnika
     * @return uchwyt do numeru pomocnika
     */
    int pomocnik();
    
    /**
     * Setter pomocnika
     * @param p numer pomocnika
     */
    void pomocnik(int p);
    
    /**
     * Metoda zwraca uchwyt do numerów pomocników już wybranych
     * @return uchwyt do numerów pomocników już wybranych
     */
    Map pomocnicyWybrani();
    
    /**
     * Metoda zwraca uchwyt do nazw kategorii już wybranych
     * @return uchwyt do nazw kategorii już wybranych
     */
    List kategorieWybrane();
    
    /**
     * Metoda zwraca bieżący wynik gracza
     * @return bieżący wynik gracza (0-10)
     */
    int wynik();
    
    /**
     * Setter wyniku
     * @param w wynik
     */
    void wynik(int w);
    
    // metoda upraszczająca zapis zamykania okna gry
    void closeOperation();
}
