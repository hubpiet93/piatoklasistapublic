package Interfejsy;

/**
 *
 * @author grobacz
 * interfejs okien pozwalajacy na schematyczne przypisywanie zdarzeń przycisków
 */
public interface PanelInit{

    public abstract void setButtons(ISilnikGry parent);
    
}
