package piatoklasistamain;

import BazaDanych.*;
import KlasyDanych.*;
import java.awt.Color;
import java.io.IOException;
import java.util.*;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JOptionPane;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pk.projekt.e2.sound.contract.IAudioManager;

/**
 *
 * @author Basia
 */
public class Logowanie extends javax.swing.JFrame {

    IAudioManager audioManager;
    KlasyDanych.Ustawienia ustawienia;

    /**
     * Creates new form Logowanie
     *
     * @param am objekt AudioManagera
     * @param u objekt Ustawień
     */
    public Logowanie(IAudioManager am, KlasyDanych.Ustawienia u) {
        initComponents();
        this.getContentPane().setBackground(Color.black);
        setLocationRelativeTo(null);
        audioManager = am;
        ustawienia = u;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextField1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jTextField1.setForeground(new java.awt.Color(204, 0, 0));

        jLabel1.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 0, 0));
        jLabel1.setText("Podaj imię");

        jButton1.setBackground(new java.awt.Color(204, 0, 0));
        jButton1.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("COFNIJ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(204, 0, 0));
        jButton2.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("GRAJ");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // powrót do menu
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (ustawienia.getKlawisze()) {
            try {
                audioManager.playSound("klikniecie");
            } catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException ex) {
                System.out.println("Blad dzwieku");
            }
        }
        ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/beans.xml");
        BeanFactory factory = context;
        Menu menu = (Menu) factory.getBean("menu");
        menu.show();
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
    
    // rozpoczęcie gry
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (ustawienia.getKlawisze()) {
            try {
                audioManager.playSound("klikniecie");
            } catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException ex) {
                System.out.println("Blad dzwieku");
            }
        }

        // przypadek pustego pola z imieniem
        if ("".equals(jTextField1.getText().trim())) {
            JOptionPane.showMessageDialog(Logowanie.this, "Proszę podać imię");
            return;
        }

        // objekt gracza wyszukany w bazie na podstawie imienia
        Gracz graczDB = new Gracz();
        IDB db = DB.getDB();
        List<Gracz> gracze = new ArrayList<>(db.PobierzDane(Gracz.class));
        for (Gracz g : gracze) {
            if (g.getImie() == null ? jTextField1.getText() == null : g.getImie().equals(jTextField1.getText())) {
                graczDB = g;
                break;
            }
        }
        // jeśli imie występuje w bazie - pytaj o to czy logować. Alternatywnie zaloguj bez pytania
        int opt = -1;
        if (graczDB.isEmpty()) {
            opt = 0;
        } else {
            opt = JOptionPane.showOptionDialog(this, "Gracz o tym imieniu istnieje. Zalogować?", "Uwaga", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
        }
        switch (opt) {
            // OK
            case 0:
                SilnikGry g = null;
                if (!graczDB.isEmpty()) {
                    g = new SilnikGry(graczDB);
                } else {
                    Gracz gracz = new Gracz(jTextField1.getText());
                    db.DodajObiekt(gracz);
                    db.Zamknij();
                    g = new SilnikGry(gracz);
                }
                g.setWholeOkno();
                g.getOknoGry().show();
                this.dispose();
                break;
            // CANCEL
            case 2:
                break;
            default:
                System.out.println(opt);
                break;
        }
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
