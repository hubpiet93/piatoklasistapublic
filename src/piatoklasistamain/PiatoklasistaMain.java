package piatoklasistamain;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author grobacz
 */
public class PiatoklasistaMain {

    /**
     * @param args the command line arguments
     * Punkt wejściowy programu, wejście do menu
     */
    public static void main(String[] args) {
        
        ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/beans.xml");
        BeanFactory factory = context;
        Menu menu = (Menu) factory.getBean("menu");
        menu.show();
    }
    
}
