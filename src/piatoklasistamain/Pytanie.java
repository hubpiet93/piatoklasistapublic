package piatoklasistamain;

import Interfejsy.ISilnikGry;
import Interfejsy.PanelInit;
import KlasyDanych.Wynik;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.util.Timer;
import java.util.TimerTask;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JPanel;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Basia
 */
public class Pytanie extends JPanel implements PanelInit {

    ISilnikGry parent;
    pytaniareader.Pytanie p;
    JRadioButton[] opcje;
    Timer timer;
    javax.swing.Timer countdown;
    int currenttime = 1;

    /**
     * Creates new form Pytanie
     *
     * @param g parent
     */
    public Pytanie(ISilnikGry g) {
        initComponents();
        parent = g;
        g.generatorDecyzji().setParameters(parent.pomocnik());
        // znaczniki html dla lepszego formatowania tekstu
        p = (pytaniareader.Pytanie) g.generatorDecyzji().wyborPytania(parent.kategoria());
        jRadioButton1.setText("<html>" + p.getOdp()[0] + "</html>");
        jRadioButton2.setText("<html>" + p.getOdp()[1] + "</html>");
        jRadioButton3.setText("<html>" + p.getOdp()[2] + "</html>");
        jRadioButton4.setText("<html>" + p.getOdp()[3] + "</html>");
        // grupa przycisków dla lepszej obsługi
        ButtonGroup group = new ButtonGroup();
        group.add(jRadioButton1);
        group.add(jRadioButton2);
        group.add(jRadioButton3);
        group.add(jRadioButton4);
        jLabel1.setText("<html>" + p.getTresc() + "</html>");
        // grupa przycisków w postaci tablicy
        opcje = new JRadioButton[]{jRadioButton1, jRadioButton2, jRadioButton3, jRadioButton4};
        // inicjalizacja odliczania i objektu obsługi zdarzenia przekroczonego limitu czasu
        timer = new Timer();
        countdown = new javax.swing.Timer(1000, (ActionEvent e) -> {
            jLabel2.setText(String.valueOf(this.currenttime));
            this.currenttime++;
        });
        setBackground(Color.black);
    }

    @Override
    public void setButtons(ISilnikGry parent) {
        // dla gry z limitem czasowym tworzy zdarzenie niepowodzenia z odpowiednim opóźnieniem
        // jeśli wybierze się wcześniej odpowiedź zdarzenie jest anulowane
        if (parent.ustawienia().getLimit() > 0) {
            int delay = 0;
            if (parent.ustawienia().getLimit() == 1) {
                delay = 60000;
            } else if (parent.ustawienia().getLimit() == 2) {
                delay = 20000;
            }
            timer.schedule(new TimerTask() {
                int i = 0;

                @Override
                public void run() {
                    zlaOdpowiedz();
                }
            }, delay);
        }
        // licznik czasu który upłynął
        countdown.start();
        this.jButton1.addActionListener((ActionEvent evt) -> {
            // nie wybrana odpowiedź
            if (Pytanie.this.getSelected() == 0) {
                if (parent.ustawienia().getKlawisze()) {
                    try {
                        parent.audioManager().playSound("klikniecie");
                    } catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException ex) {
                        System.out.println("Blad dzwieku");
                    }
                }
                JOptionPane.showMessageDialog(Pytanie.this, "Proszę zaznaczyć odpowiedź");
                return;
            }
            // anulowanie licznika czasu i zdarznia niepowodzenia
            timer.cancel();
            timer.purge();
            countdown.stop();
            // dźwięk klik tylko gdy nie są włączone inne dźwięki (synchroniczne odtwarzanie)
            if (parent.ustawienia().getGlosnosc() == 0 && parent.ustawienia().getKlawisze()) {
                try {
                    parent.audioManager().playSound("klikniecie");
                } catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException ex) {
                    System.out.println("Blad dzwieku");
                }
            }
            // prawidłowa odpowiedź
            if (p.getPoprawna() == Pytanie.this.getSelected()) {
                JRadioButton b = opcje[Pytanie.this.getSelected() - 1];
                b.setBackground(Color.green);
                if (parent.ustawienia().getGlosnosc() != 0) {
                    try {
                        parent.audioManager().playSound("brawa");
                    } catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException ex) {
                        System.out.println("Blad dzwieku");
                    }
                }
                JOptionPane.showMessageDialog(Pytanie.this, "Brawo! Prawidłowa odpowiedź");
                parent.wynik(parent.wynik() + 1);
                parent.setOkno(new Kategoria(parent));
                parent.refresh();
            } // nieprawidłowa odpowiedź
            else {
                JRadioButton b = opcje[Pytanie.this.getSelected() - 1];
                b.setBackground(Color.red);
                JRadioButton pop = opcje[p.getPoprawna() - 1];
                pop.setBackground(Color.green);
                zlaOdpowiedz();
            }
        });
    }

    // operacja gotwca wywołana z lewego panelu

    public void gotowiec() {
        opcje[p.getPoprawna() - 1].setBackground(Color.green);
    }

    // operacja ściągi wywołana z lewego panelu

    public void sciaga() {
        // użycie generatora decyzji dla uzyskania informacji czy pomocnik udzielił prawidłowej odpowiedzi
        if (parent.generatorDecyzji().czySukces()) {
            opcje[p.getPoprawna() - 1].setBackground(Color.yellow);
        } else {
            List tmp = new ArrayList();
            tmp.add(1);
            tmp.add(2);
            tmp.add(3);
            tmp.add(4);
            tmp.remove((int) (p.getPoprawna() - 1));
            Random r = new Random();
            opcje[(int) tmp.get(r.nextInt(3)) - 1].setBackground(Color.yellow);
        }
    }

    // nieprawidłowa odpowiedź lub czas upłynął

    private void zlaOdpowiedz() {
        // zatrzymaj zdarzenia i licznik
        timer.cancel();
        timer.purge();
        countdown.stop();
        // jeśli rtunek niewykorzystany to przejdź do modułu
        if (parent.isRatunek()) {
            if (parent.ustawienia().getGlosnosc() != 0) {
                try {
                    parent.audioManager().playSound("blad");
                } catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException ex) {
                    System.out.println("Blad dzwieku");
                }
            }
            parent.setOkno(new Ratunek(parent, p));
            parent.refresh();
        } // w przeciwnym przypadku niepowodzenie -> menu
        else {
            if (parent.ustawienia().getGlosnosc() != 0) {
                try {
                    parent.audioManager().playSound("przegrana");
                } catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException ex) {
                    System.out.println("Blad dzwieku");
                }
            }
            JOptionPane.showMessageDialog(Pytanie.this, "Niestety tym razem się nie udało");
            parent.db().DodajObiekt(new Wynik(parent.gracz().getImie(), parent.wynik()));
            parent.db().Zamknij();
            ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/beans.xml");
            BeanFactory factory = context;
            Menu menu = (Menu) factory.getBean("menu");
            menu.show();
            parent.getOknoGry().dispose();
        }
    }

    // funkcja pomocnicza zwracająca numer wybranej odpowiedzi 1-4 lub 0 gdy nic nie wybrano
    int getSelected() {
        int i = 1;
        for (JRadioButton b : opcje) {
            if (b.isSelected()) {
                return i;
            }
            i++;
        }
        return 0;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(469, 285));
        setMinimumSize(new java.awt.Dimension(469, 285));
        setPreferredSize(new java.awt.Dimension(545, 414));

        jLabel1.setFont(new java.awt.Font("Verdana", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 0, 0));
        jLabel1.setText("Symbolem Białowieskiego Parku Narodowego jest:");

        jRadioButton1.setBackground(new java.awt.Color(0, 0, 0));
        jRadioButton1.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jRadioButton1.setForeground(new java.awt.Color(204, 0, 0));
        jRadioButton1.setText("Żubr");
        jRadioButton1.setPreferredSize(new java.awt.Dimension(80, 250));

        jRadioButton2.setBackground(new java.awt.Color(0, 0, 0));
        jRadioButton2.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jRadioButton2.setForeground(new java.awt.Color(204, 0, 0));
        jRadioButton2.setText("Ryś");
        jRadioButton2.setPreferredSize(new java.awt.Dimension(70, 250));

        jRadioButton3.setBackground(new java.awt.Color(0, 0, 0));
        jRadioButton3.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jRadioButton3.setForeground(new java.awt.Color(204, 0, 0));
        jRadioButton3.setText("Żbik");
        jRadioButton3.setPreferredSize(new java.awt.Dimension(70, 250));

        jRadioButton4.setBackground(new java.awt.Color(0, 0, 0));
        jRadioButton4.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jRadioButton4.setForeground(new java.awt.Color(204, 0, 0));
        jRadioButton4.setText("Jeleń");
        jRadioButton4.setPreferredSize(new java.awt.Dimension(70, 250));

        jButton1.setBackground(new java.awt.Color(204, 0, 0));
        jButton1.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("ZATWIERDŹ");

        jLabel2.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 0, 0));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel3.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(204, 0, 0));
        jLabel3.setText("Odpowiadasz na pytanie:");

        jLabel4.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(204, 0, 0));
        jLabel4.setText("s");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jRadioButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jRadioButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jRadioButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jRadioButton4, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
            .addGroup(layout.createSequentialGroup()
                .addGap(141, 141, 141)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jRadioButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                    .addComponent(jRadioButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    // End of variables declaration//GEN-END:variables
}
