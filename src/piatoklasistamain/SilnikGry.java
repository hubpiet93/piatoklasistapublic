package piatoklasistamain;

import BazaDanych.DB;
import BazaDanych.IDB;
import KlasyDanych.Gracz;
import Interfejsy.PanelInit;
import KlasyDanych.Ustawienia;
import KlasyDanych.Wynik;
import generator.IDecyzje;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pk.projekt.e2.sound.contract.IAudioManager;

/**
 *
 * @author grobacz
 */
public class SilnikGry implements Interfejsy.ISilnikGry {
    
    private final JFrame okno;
    private IDB db = DB.getDB();
    private PanelInit operacja;
    private final Gracz gracz;
    private String kategoria;
    private int pomocnik;
    private final Map pomocnicyWybrani = new HashMap();
    private final List kategorieWybrane = new ArrayList<>();
    private int wynik = 0;
    private final generator.IDecyzje generatorDecyzji;
    private Lewy lewy;
    private Gorny gorny;
    private Boolean sciaga = true;
    private Boolean gotowiec = true;
    private Boolean ratunek = true;
    private final IAudioManager audioManager;
    private final KlasyDanych.Ustawienia ustawienia;
    
    /**
     * Konstruktor SilnikaGry
     * @param gracz Objekt gracza logującego się do gry
     */
    public SilnikGry(Gracz gracz){
        this.gracz=gracz;
        JFrame f = new JFrame("Are you smarter than 5th grader?");
        
        // zdarzenie kliknięcia x w grze
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        f.addWindowListener(
            new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e){
                    db.Zamknij();
                    int conf = JOptionPane.showOptionDialog(
                        e.getComponent(), 
                        "Wrócić do menu czy zamknąć?", 
                        "Zamykanie",
                        JOptionPane.NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        new Object[]{"Menu","Zamknij"},
                        0
                    );
                    if(conf == JOptionPane.OK_OPTION)
                        closeOperation();
                    else{
                        System.exit(0);
                    }
                }
        });
        
        // referencja okna gry
        okno = f;
        
        // pobranie ustawień
        ustawienia = (KlasyDanych.Ustawienia)db.PobierzDane(KlasyDanych.Ustawienia.class).get(0);
        generatorDecyzji = new generator.Decyzje(ustawienia.getPoziomTrudnosci());        
        
        // konfiguracja komponentu audio dla sekcji gry, kliknięcie jest już skonfigurowane w beanach
        ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/beans.xml");
        BeanFactory factory = context;
        audioManager = (IAudioManager)factory.getBean("audioMan");
        audioManager.addSound("blad", "./baza/dzwieki/blad.wav");
        audioManager.addSound("brawa", "./baza/dzwieki/brawa.wav");
        audioManager.addSound("brawa_na_koniec", "./baza/dzwieki/brawa_na_koniec.wav");
        audioManager.addSound("przegrana", "./baza/dzwieki/przegrana.wav");
        
    }
    
    // metoda wywołana przy uruchomieniu
    @Override
    public void setWholeOkno(){
        setWholeOkno(new Kategoria(this));
    }
    
    // metoda wywołana przy uruchomieniu - stworznie okna gry
    // składane są panele górny, lewy i panel bieżącej operacji
    private void setWholeOkno(PanelInit operacja) {
        JPanel p = new JPanel(new BorderLayout());
        gorny = new Gorny();
        p.add(gorny,BorderLayout.NORTH);
        lewy = new Lewy(this);
        p.add(lewy,BorderLayout.WEST);
        operacja.setButtons(this);
        p.add((JPanel)operacja,BorderLayout.CENTER);
        okno.setContentPane(p);
        okno.pack();
        
        // pozycja bliżej środka ekranu, bez zmian w rozmiarze okna
        okno.setBounds(300,50,okno.getWidth(),okno.getHeight());
    }
    
    // metoda uniwersalna zmieniająca zawartość okna gry na bieżącą operację
    // składane są panele górny, lewy i panel bieżącej operacji z aktualnymi danymi
    @Override
    public void setOkno(PanelInit operacja) {
        if(wynik == 10){
            operacja = new Wygrana();
            db.DodajObiekt(new Wynik(gracz.getImie(),wynik));
        }
        operacja.setButtons(this);
        this.operacja = operacja;
        okno.getContentPane().remove(2);
        okno.getContentPane().add((JPanel)operacja,BorderLayout.CENTER);
        lewy.setScore(wynik);
        if(operacja instanceof Pytanie){
            gorny.setPomocnik(pomocnik);
        }
        else if((pomocnicyWybrani.containsKey(pomocnik)&&(int)pomocnicyWybrani.get(pomocnik)!=1)||!pomocnicyWybrani.containsKey(pomocnik)){
            gorny.setDefaultPomocnik();
        }
    }
    
    // metoda upraszczająca zapis
    // aktualizuje komponenty wizualne okna
    @Override
    public void refresh(){
        okno.revalidate();
        okno.repaint();
    }
    
    // metoda upraszczająca zapis
    @Override
    public void closeOperation(){
        okno.setVisible(false);
        okno.dispose();
        ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/beans.xml");
        BeanFactory factory = context;
        Menu menu = (Menu) factory.getBean("menu");
        menu.show();
    }
    
    // akcesory ratunku, gotowca i ściągi
    
    @Override
    public void disableRatunek(){
        ratunek = false;
    }
    
    @Override
    public Boolean isSciaga(){
        return sciaga;
    }
    
    @Override
    public Boolean isGotowiec(){
        return gotowiec;
    }
    
    @Override
    public Boolean isRatunek(){
        return ratunek;
    }

    @Override
    public JFrame getOknoGry() {
        return okno;    
    }

    @Override
    public IAudioManager audioManager() {
        return audioManager;
    }

    @Override
    public Ustawienia ustawienia() {
        return ustawienia;
    }

    @Override
    public IDB db() {
        return db;
    }

    @Override
    public Gracz gracz() {
        return gracz;
    }

    @Override
    public IDecyzje generatorDecyzji() {
        return generatorDecyzji;
    }

    @Override
    public String kategoria() {
        return kategoria;
    }

    @Override
    public int pomocnik() {
        return pomocnik;
    }

    @Override
    public Map pomocnicyWybrani() {
        return pomocnicyWybrani;
    }

    @Override
    public List kategorieWybrane() {
        return kategorieWybrane;
    }

    @Override
    public int wynik() {
        return wynik;
    }

    @Override
    public void kategoria(String k) {
        kategoria=k;
    }

    @Override
    public void pomocnik(int p) {
        pomocnik=p;
    }

    @Override
    public void wynik(int w) {
        wynik=w;
    }

    @Override
    public Boolean isPytanie() {
        return operacja instanceof Pytanie;
    }

    @Override
    public void sciagaj() {
        if(isPytanie()) ((Pytanie)operacja).sciaga();
        sciaga = false;
    }

    @Override
    public void patrzNaGotowiec() {
        if(isPytanie()) ((Pytanie)operacja).gotowiec();
        gotowiec = false;
    }
}
