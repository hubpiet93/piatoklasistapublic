echo "Copy/Overwrite"

del build /Y
del PiatoklasistaMain.jar

mkdir build
mkdir build\classes
mkdir build\classes\GUI
mkdir build\classes\piatoklasistamain
mkdir build\classes\KlasyDanych
mkdir build\classes\META-INF

copy .\src\GUI\* .\build\classes\GUI\ /Y
copy .\src\piatoklasistamain\* .\build\classes\piatoklasistamain\ /Y
copy .\src\KlasyDanych\* .\build\classes\KlasyDanych\ /Y
copy .\src\META-INF\beans.xml .\build\classes\META-INF\ /Y

echo "Compile"

javac .\build\classes\KlasyDanych\*.java
del .\build\classes\KlasyDanych\*.java
javac -encoding utf8 .\build\classes\piatoklasistamain\*.java
del .\build\classes\piatoklasistamain\*.java
del .\build\classes\piatoklasistamain\*.form

echo "Create jar"
cd .\build\classes\
jar cmf ..\..\manifest.mf ..\..\PiatoklasistaMain.jar GUI KlasyDanych piatoklasistamain META-INF

cd ..\..\
echo "Start jar"
java -jar PiatoklasistaMain.jar
pause